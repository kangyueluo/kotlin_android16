package com.wordpress.smdaudhilbe.androiduipageviewcontroller

import java.util.ArrayList

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v4.view.ViewPager.OnPageChangeListener
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView

class MainActivity : Activity() {

    private var viewPager: ViewPager? = null
    lateinit var myViewPagerAdapter: MyViewPagerAdapter
    private var listOfItems: ArrayList<Int>? =  ArrayList()


    private lateinit var dotsLayout: LinearLayout
    private var dotsCount: Int = 0
    private lateinit var dots: Array<TextView?>

    //	page change listener
    internal var viewPagerPageChangeListener: OnPageChangeListener = object : OnPageChangeListener {

        override fun onPageSelected(position: Int) {
            for (i in 0 until dotsCount) {
                dots!![i]!!.setTextColor(resources.getColor(android.R.color.darker_gray))
            }
            dots!![position]!!.setTextColor(resources.getColor(R.color.app_green))
        }

        override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {

        }

        override fun onPageScrollStateChanged(arg0: Int) {

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()
        setViewPagerItemsWithAdapter()
        setUiPageViewController()
    }

    private fun setUiPageViewController() {
        dotsLayout = findViewById<View>(R.id.viewPagerCountDots) as LinearLayout
        dotsCount = myViewPagerAdapter!!.count
        dots = arrayOfNulls<TextView>(dotsCount)

        for (i in 0 until dotsCount) {
            dots[i] = TextView(this)
            dots!![i]!!.text = Html.fromHtml("&#8226;")
            dots!![i]!!.textSize = 30f
            dots!![i]!!.setTextColor(resources.getColor(android.R.color.darker_gray))
            dotsLayout!!.addView(dots!![i])
        }

        dots!![0]!!.setTextColor(resources.getColor(R.color.app_green))
    }

    private fun setViewPagerItemsWithAdapter() {
        myViewPagerAdapter = MyViewPagerAdapter(listOfItems!!)
        viewPager!!.adapter = myViewPagerAdapter
        viewPager!!.currentItem = 0
        viewPager!!.setOnPageChangeListener(viewPagerPageChangeListener)
    }

    private fun initViews() {

        actionBar!!.hide()

        viewPager = findViewById<View>(R.id.viewPager) as ViewPager

        listOfItems!!.add(1)
        listOfItems!!.add(2)
        listOfItems!!.add(3)
        listOfItems!!.add(4)
        listOfItems!!.add(5)
    }

    //	adapter
    inner class MyViewPagerAdapter(private val items: ArrayList<Int>) : PagerAdapter() {

        private var layoutInflater: LayoutInflater? = null

        override fun instantiateItem(container: ViewGroup, position: Int): Any {

            layoutInflater = applicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view = layoutInflater!!.inflate(R.layout.view_pager_item, container, false)

            val tView = view.findViewById<View>(R.id.PageNumber) as TextView

            tView.text = listOfItems!![position].toString()

            (container as ViewPager).addView(view)

            return view
        }

        override fun getCount(): Int {
            return items.size
        }

        override fun isViewFromObject(view: View, obj: Any): Boolean {
            return view === obj as View
        }


        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            val view = `object` as View
            (container as ViewPager).removeView(view)
        }
    }
}